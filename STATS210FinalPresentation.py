import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

"""
# R-code for loading and saving the hormone database in .csv format
library("bootstrap")
write.csv(hormone, 'hormone.csv')
"""

np.random.seed(42)
# Reading the hormone dataset Page 107 : Efron Book
hormone_data = pd.read_csv('hormone.csv', delimiter=',', index_col=0)

# Figure 9.1
fig = plt.figure()
ax1 = plt.axes(frameon=True)

# removing top and right part of frame
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)

# setting ticks position
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.scatter(hormone_data[hormone_data['Lot'] == 'A']['hrs'], hormone_data[hormone_data['Lot'] == 'A']['amount'], c='r')
plt.scatter(hormone_data[hormone_data['Lot'] == 'B']['hrs'], hormone_data[hormone_data['Lot'] == 'B']['amount'], c='g')
plt.scatter(hormone_data[hormone_data['Lot'] == 'C']['hrs'], hormone_data[hormone_data['Lot'] == 'C']['amount'], c='b')
plt.legend(['A', 'B', 'C'], fontsize=28)
plt.xlabel('Hours', fontsize=28)
plt.ylabel('Amount', fontsize=28)
plt.rc('legend', fontsize=28)
plt.tick_params(labelsize=28)

# Problem 17.8

"""
(a) Carry out a bootstrap analysis for the hormone data,
like the one in Table 17.1, using B = 100 bootstrap samples.
In addition, calculate the average prediction error €0 for observations that do not appear in the bootstrap
sample used for their prediction. Hence compute the .632
estimator for these data.
"""
from sklearn.linear_model import LinearRegression

len_data = len(hormone_data)
X_lotA = np.array(hormone_data[hormone_data['Lot'] == 'A']['hrs']).reshape(-1,1)
X_lotB = np.array(hormone_data[hormone_data['Lot'] == 'B']['hrs']).reshape(-1,1)
X_lotC = np.array(hormone_data[hormone_data['Lot'] == 'C']['hrs']).reshape(-1,1)

y_lotA = np.array(hormone_data[hormone_data['Lot'] == 'A']['amount']).reshape(-1,1)
y_lotB = np.array(hormone_data[hormone_data['Lot'] == 'B']['amount']).reshape(-1,1)
y_lotC = np.array(hormone_data[hormone_data['Lot'] == 'C']['amount']).reshape(-1,1)



from sklearn.metrics import mean_squared_error

X = np.zeros((27, 4))

X[:,0] = np.array(hormone_data['hrs'], dtype=np.float)
X[0:9, 1] = np.ones((9))
X[9:18,2] = np.ones((9))
X[18:27,3] = np.ones((9))

y = np.array(hormone_data['amount']).reshape(-1, 1)


regr = LinearRegression(fit_intercept=False)

regr.fit(X, y)
RSE = mean_squared_error(y, regr.predict(X))




"""
RSE = 0
# Lot A
regr_lotA = LinearRegression()
regr_lotA.fit(X_lotA, y_lotA)
y_pred_lotA = regr_lotA.predict(X_lotA)
RSE += np.sum((y_lotA - y_pred_lotA)**2)

# Lot B
regr_lotB = LinearRegression()
regr_lotB.fit(X_lotB, y_lotB)
y_pred_lotB = regr_lotB.predict(X_lotB)
RSE += np.sum((y_lotB - y_pred_lotB)**2)

# Lot C
regr_lotC = LinearRegression()
regr_lotC.fit(X_lotC, y_lotC)
y_pred_lotC = regr_lotC.predict(X_lotC)
RSE += np.sum((y_lotC - y_pred_lotC)**2)

RSE = RSE/len_data
"""

# Naive Bootstrap Estimate
# Bootstrap Samples

B = 400
NaiveBSResults_df = pd.DataFrame(index=np.arange(B), columns=['Prediction Error', 'Apparent Error', 'Optimism'], dtype=np.float)
NaiveBS_df = pd.DataFrame(index=np.arange(1, len_data+1), columns=np.arange(B))

for rep in range(B):

    b_idxs = np.random.choice(a=np.arange(1, len_data+1), size=len_data, replace=True)
    NaiveBS_df.loc[:, rep] = b_idxs
    b_data = hormone_data.loc[b_idxs]

    # Fitting each lot
    PE_original = 0
    PE_boot = 0

    X_boot = np.zeros((27,4))
    X_boot[:, 0] = np.array(b_data['hrs'], dtype=np.float)

    y_boot = np.array(b_data['amount'], dtype=np.float)

    lot_data = list(b_data['Lot'])

    for idx in range(len(lot_data)):
        if lot_data[idx] == 'A':
            X_boot[idx, 1] = 1
        elif lot_data[idx] == 'B':
            X_boot[idx, 2] = 1
        elif lot_data[idx] == 'C':
            X_boot[idx, 3] = 1

    regr = LinearRegression(fit_intercept=False)
    regr.fit(X_boot, y_boot)

    y_pred = regr.predict(X)
    y_pred_boot = regr.predict(X_boot)

    PE_original = mean_squared_error(y_true=y, y_pred=y_pred)
    PE_boot = mean_squared_error(y_true=y_boot, y_pred=y_pred_boot)

    NaiveBSResults_df.loc[rep, 'Prediction Error'] = PE_original
    NaiveBSResults_df.loc[rep, 'Apparent Error'] = PE_boot
    NaiveBSResults_df.loc[rep, 'Optimism'] = PE_original - PE_boot


NaiveBSResults_df.mean()

"""
# Refined Bootstrap Estimate
B = 10
RefinedBSResults_df = pd.DataFrame(index=np.arange(B), columns=['Prediction Error', 'Apparent Error', 'Optimism'], dtype=np.float)
RefinedBS_df = pd.DataFrame(index=np.arange(1, len_data+1), columns=np.arange(B))

for rep in range(B):

    b_idxs = np.random.choice(a=np.arange(1, len_data+1), size=len_data, replace=True)
    RefinedBS_df.loc[:, rep] = b_idxs

    b_data = hormone_data.loc[b_idxs]

    # Fitting each lot
    PE_original = 0
    PE_boot = 0

    for lot in ['A', 'B', 'C']:

        X_boot = np.array(b_data[b_data['Lot'] == lot]['hrs']).reshape(-1,1)
        y_boot = np.array(b_data[b_data['Lot'] == lot]['amount']).reshape(-1,1)

        regr = LinearRegression()
        regr.fit(X_boot, y_boot)

        # taking uniques b_idx (unique observations)
        unique_b_idxs = np.unique(b_idxs)
        obs_data = hormone_data.loc[unique_b_idxs]

        X_lot = np.array(obs_data[obs_data['Lot'] == lot]['hrs']).reshape(-1,1)
        y_lot = np.array(obs_data[obs_data['Lot'] == lot]['amount']).reshape(-1,1)

        y_pred = regr.predict(X_lot)
        y_pred_boot = regr.predict(X_boot)

        PE_original += np.sum((y_lot - y_pred)**2)
        PE_boot += np.sum((y_boot - y_pred_boot)**2)

    PE_original = PE_original/len(unique_b_idxs)  # On unique observations
    PE_boot = PE_boot/len(b_idxs)  # On all bootstrap observations

    RefinedBSResults_df.loc[rep, 'Prediction Error'] = PE_original
    RefinedBSResults_df.loc[rep, 'Apparent Error'] = PE_boot
    RefinedBSResults_df.loc[rep, 'Optimism'] = PE_original - PE_boot


RefinedBSResults_df.mean()
"""

RefinedBSEstimate = RSE + NaiveBSResults_df["Optimism"].mean()

# Calculation epsilon0
eps_dict = {x:[] for x in np.arange(1, len_data+1)}

for obs in np.arange(1, len_data+1):  # Looping over each observation
    for b in range(B):  # Looping over bootstrap sample
        if obs not in np.array(NaiveBS_df[b]): # Check if the observation appear in corresponding bootstrap sample
            eps_dict[obs].append(b)


eps_err = []
for eps in eps_dict:

    err = 0
    for bs_sample_idx in eps_dict[eps]:

        b_idxs = NaiveBS_df[bs_sample_idx]

        b_data = hormone_data.loc[b_idxs]

        X_boot = np.zeros((27, 4))
        X_boot[:, 0] = np.array(b_data['hrs'], dtype=np.float)

        y_boot = np.array(b_data['amount'], dtype=np.float)

        lot_data = list(b_data['Lot'])

        for idx in range(len(lot_data)):
            if lot_data[idx] == 'A':
                X_boot[idx, 1] = 1
            elif lot_data[idx] == 'B':
                X_boot[idx, 2] = 1
            elif lot_data[idx] == 'C':
                X_boot[idx, 3] = 1

        regr = LinearRegression(fit_intercept=False)
        regr.fit(X_boot, y_boot)

        y_pred = regr.predict(X[eps-1,:].reshape(1, -1))

        err += (y[eps-1] - y_pred)**2

    if len(eps_dict[eps]) > 0:
        err = err/len(eps_dict[eps])
    eps_err.append(err)

eps_0 = np.mean(eps_err)


estimator_632 = 0.368 * RSE + 0.632 * eps_0


print("Prediction Error: %f" % RSE)
print("Bootstrap Estimate of Prediction Error: %f" % NaiveBSResults_df['Prediction Error'].mean())
print("Refined Bootstrap estimate of prediction Error: %f" % RefinedBSEstimate)
print("Average error rate from bootstrap data sets not containing the point being predicted: %f" % eps_0)
print(".632 estimate of prediction error: %f" % estimator_632)



# Calculation epsilon0, epsilon1, ....
epsilon_range = 10
eps_dict_list = [{x:[] for x in np.arange(1, len_data+1)} for i in range(epsilon_range)]

for i in range(epsilon_range):
    eps_dict = eps_dict_list[i]

    for obs in np.arange(1, len_data+1):  # Looping over each observation
        for b in range(B):  # Looping over bootstrap sample
            if i == list(NaiveBS_df[b]).count(obs): # Check if the observation appear in corresponding bootstrap sample
                eps_dict[obs].append(b)

eps_err_list = [0 for i in range(epsilon_range)]

for i in range(epsilon_range):

    eps_dict = eps_dict_list[i]

    eps_err = []
    for eps in eps_dict:

        err = 0
        for bs_sample_idx in eps_dict[eps]:

            b_idxs = NaiveBS_df[bs_sample_idx]

            b_data = hormone_data.loc[b_idxs]

            X_boot = np.zeros((27, 4))
            X_boot[:, 0] = np.array(b_data['hrs'], dtype=np.float)

            y_boot = np.array(b_data['amount'], dtype=np.float)

            lot_data = list(b_data['Lot'])

            for idx in range(len(lot_data)):
                if lot_data[idx] == 'A':
                    X_boot[idx, 1] = 1
                elif lot_data[idx] == 'B':
                    X_boot[idx, 2] = 1
                elif lot_data[idx] == 'C':
                    X_boot[idx, 3] = 1

            regr = LinearRegression(fit_intercept=False)
            regr.fit(X_boot, y_boot)

            y_pred = regr.predict(X[eps-1,:].reshape(1, -1))

            err += (y[eps-1] - y_pred)**2

        if len(eps_dict[eps]) > 0:
            err = err/len(eps_dict[eps])
        eps_err.append(err)

    eps_err_list[i] = np.mean(eps_err)


plt.figure()

# Figure 9.1

fig = plt.figure()
ax1 = plt.axes(frameon=True)

# removing top and right part of frame
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)

# setting ticks position
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

plt.scatter(range(epsilon_range), eps_err_list)
plt.plot(range(epsilon_range), eps_err_list)

plt.xlabel('$j$', fontsize=28)
plt.ylabel('$\hat{\epsilon_j}$', fontsize=28)
plt.rc('legend', fontsize=28)
plt.tick_params(labelsize=28)
plt.xlim(0)
plt.ylim(0)
